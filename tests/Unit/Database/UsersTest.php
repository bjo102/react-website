<?php

namespace Tests\Unit\Database;

use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class UsersTest extends TestCase
{
    /**
     * A basic unit test example.
     */
    public function test_user_table_has_expected_columns(): void
    {
        $this->assertTrue(
            Schema::hasColumns('users', [
                'id','name', 'email', 'email_verified_at', 'password', 'timezone'
        ]), 1);
    }

    public function test_id_is_integer(): void
    {
        $this->assertTrue(Schema::getColumnType('users', 'id') === 'bigint', 1);
    }

    public function test_name_is_string(): void
    {
        $this->assertTrue(Schema::getColumnType('users', 'name') === 'varchar', 1);
    }

    public function test_email_is_string(): void
    {
        $this->assertTrue(Schema::getColumnType('users', 'email') === 'varchar', 1);
    }

    public function test_email_verified_at_is_timestamp(): void
    {
        $this->assertTrue(Schema::getColumnType('users', 'email_verified_at') === 'timestamp', 1);
    }

    public function test_password_is_string(): void
    {
        $this->assertTrue(Schema::getColumnType('users', 'password') === 'varchar', 1);
    }

    public function test_timezone_is_string(): void
    {
        $this->assertTrue(Schema::getColumnType('users', 'timezone') === 'varchar', 1);
    }

    public function test_user_table_has_expected_primary_key(): void
    {
        $this->assertTrue(Schema::hasPrimary('users', 'id'), 1);
    }

    public function test_user_table_has_expected_indexes(): void
    {
        $this->assertTrue(Schema::hasIndex('users', 'users_email_unique'), 1);
    }
}
