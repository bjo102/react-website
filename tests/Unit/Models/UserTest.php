<?php

namespace Tests\Unit\Models;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Generator;

class UserTest extends TestCase
{

    public function testCreateUsers(): void
    {
        $factory->define(User::class, function (Generator $faker) {
            return [
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => bcrypt(str_random(10)),
                'remember_token' => str_random(10),
            ];
        });
    }

    public function test_create_function(): void
    {
        $author = factory(User::class)->create([
            'first_name' => 'Robert',
            'last_name' => 'Jordan'
        ]);

        $this->assertEquals('Jordan, Robert', $author->name);
    }
}
