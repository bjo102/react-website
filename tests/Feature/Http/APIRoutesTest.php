<?php

namespace Tests\Feature\Http;
use Tests\TestCase;

class APIRoutesTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->json('POST', '/user', ['name' => 'Sally'])
             ->seeJsonEquals([
                 'created' => true,
             ]);
    }

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testStructure()
    {
        $this->get('/user/1')
             ->seeJsonStructure([
                 'name',
                 'pet' => [
                     'name', 'age'
                 ]
             ]);
    }

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testUsers()
    {
        // Assert that each user in the list has at least an id, name and email attribute.
        $this->get('/users')
             ->seeJsonStructure([
                 '*' => [
                     'id', 'name', 'email'
                 ]
             ]);
    }

    public function testUsersWithRelations()
    {
        $this->get('/users')
             ->seeJsonStructure([
                 '*' => [
                     'id', 'name', 'email', 'pets' => [
                         '*' => [
                             'name', 'age'
                         ]
                     ]
                 ]
             ]);
    }

}
