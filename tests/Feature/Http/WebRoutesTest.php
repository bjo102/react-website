<?php

namespace Tests\Feature\Http;

class WebRoutesTest
{

    public function test_homepage(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

}
