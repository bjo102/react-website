export default function Article( {quote} ) {
    return (
        <h1 className="flex justify-center">
            {quote}
        </h1>
    )
}
