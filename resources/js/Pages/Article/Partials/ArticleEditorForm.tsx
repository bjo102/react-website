import {FormEventHandler, useRef } from "react";
import { Editor } from '@tinymce/tinymce-react';
import {useForm, usePage} from "@inertiajs/react";
import TextInput from "@/Components/TextInput";
import TextArea from "@/Components/TextArea";
import InputLabel from "@/Components/InputLabel";
import PrimaryButton from "@/Components/PrimaryButton";
import {Transition} from "@headlessui/react";

export default function ArticleEditorForm({ article }: { className?: string }) {
    const {data, setData, errors, put, reset, processing, recentlySuccessful } = useForm({
        name: '',
        title: '',
        excerpt: '',
        content: '',
    });
    const editorRef = useRef(null);
    const { apiKeys } = usePage().props
    const TMCEKey : string = apiKeys.tinyMCE;

    const myCustomOnChangeHandler = (content, editor) => {
    }

    const tinyConfig = {
        height: 500,
        menubar: false,
        plugins: [
            'advlist', 'autolink', 'lists', 'link', 'image', 'charmap', 'preview',
            'anchor', 'searchreplace', 'visualblocks', 'code', 'fullscreen',
            'insertdatetime', 'media', 'table', 'code', 'help', 'wordcount'
        ],
        toolbar: 'undo redo | blocks | ' +
            'bold italic forecolor | alignleft aligncenter ' +
            'alignright alignjustify | bullist numlist outdent indent | ' +
            'removeformat | help',
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
        onchange_callback : "myCustomOnChangeHandler"
    }
    const log = () => {
        if (editorRef.current) {
            console.log(editorRef.current.getContent());
        }
    };
    const submitForm: FormEventHandler = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (editorRef.current) {
            console.log(editorRef.current.getContent());
        }

        console.log(data)
    };
    return(
        <form onSubmit={submitForm} className="mt-6 space-y-6">
            <div>
                <InputLabel htmlFor="name" value="Name" />

                <TextInput
                    id="name"
                    className="mt-1 block w-full"
                    value={data.name}
                    onChange={(e) => setData('name', e.target.value)}
                    required
                    isFocused
                    autoComplete="name"
                />
            </div>

            <div>
                <InputLabel htmlFor="name" value="Title" />

                <TextInput
                    id="title"
                    className="mt-1 block w-full"
                    value={data.title}
                    onChange={(e) => setData('title', e.target.value)}
                    required
                    isFocused
                    autoComplete="title"
                />
            </div>

            <div>
                <InputLabel htmlFor="excerpt" value="Excerpt" />
                <TextArea
                    id="excerpt"
                    className="mt-1 block w-full"
                    value={data.excerpt}
                    onChange={(e) => setData('excerpt', e.target.value)}
                    required
                    isFocused
                    autoComplete="excerpt"
                />
            </div>

            <div>
                <Editor
                    apiKey={TMCEKey}
                    onInit={(evt, editor) => editorRef.current = editor}
                    onEditorChange={(text) => setData('content', text)}
                    // initialValue="<p>This is the initial content of the editor.</p>"
                    init={tinyConfig}
                />
            </div>

            <div className="flex items-center gap-4">
                <PrimaryButton disabled={processing}>Add New</PrimaryButton>

                <Transition
                    show={recentlySuccessful}
                    enter="transition ease-in-out"
                    enterFrom="opacity-0"
                    leave="transition ease-in-out"
                    leaveTo="opacity-0"
                >
                    <p className="text-sm text-gray-600 dark:text-gray-400">Saved.</p>
                </Transition>
            </div>
        </form>
    )
};
