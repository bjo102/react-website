import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import {Head} from "@inertiajs/react";
import {PageProps} from "@/types";

export default function ArticleIndex({ auth, status }: PageProps<{ status?: string }>) {
    return(
        <AuthenticatedLayout
            user={auth.user}
            header={<h2 className="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">Article</h2>}
        >
            <Head title="Profile" />
        <p>test index</p>
        </AuthenticatedLayout>
    )
}
