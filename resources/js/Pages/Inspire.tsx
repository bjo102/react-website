export default function Inspire( {quote} ) {
    return (
        <h1 className="text-white flex justify-center">
            {quote}
        </h1>
    )
}
