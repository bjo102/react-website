import GuestLayout from '@/Layouts/GuestLayout';
import { Head } from '@inertiajs/react';
import LoginForm from "@/Components/Forms/LoginForm";

export default function Login({ status, canResetPassword }: { status?: string, canResetPassword: boolean }) {
    return (
        <GuestLayout>
            <Head title="Log in" />
            <LoginForm status={status} canResetPassword={canResetPassword}/>
        </GuestLayout>
    );
}
