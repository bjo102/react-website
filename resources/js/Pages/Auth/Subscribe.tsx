import {PageProps} from "@/types";
import {useEffect, FormEventHandler} from "react";
import { Head, Link, useForm } from "@inertiajs/react";
import GuestLayout from '@/Layouts/GuestLayout';
import SubscribeForm from "@/Components/Forms/SubscribeForm";

export default function Subscribe({}) {
    return (
        <GuestLayout>
            <Head title="Subscribe" />
            <SubscribeForm />
        </GuestLayout>
    );
}
