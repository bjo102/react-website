import './bootstrap';
import '../css/app.css';

import { Provider } from 'react-redux'
import store from '@/store'
import { createRoot } from 'react-dom/client';
import { createInertiaApp } from '@inertiajs/react';
import { resolvePageComponent } from 'laravel-vite-plugin/inertia-helpers';

const appName = import.meta.env.VITE_APP_NAME || 'Laravel';

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) => resolvePageComponent(`./Pages/${name}.tsx`, import.meta.glob('./Pages/**/*.tsx')),
    setup({ el, App, props }) {
        const root = createRoot(el);

        root.render(
            // <React.StrictMode>
            <Provider store={store}>
                <App {...props} />
            </Provider>
            // </React.StrictMode>
        );
        // root.render(<App {...props} />);
    },
    progress: {
        color: '#4B5563',
    }, // testing push
});
