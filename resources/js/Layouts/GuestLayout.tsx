import ApplicationLogo from '@/Components/ApplicationLogo';
import {Link, usePage} from '@inertiajs/react';
import { PropsWithChildren } from 'react';
import Navigation from "@/Components/Navigation";

export default function Guest({ children }: PropsWithChildren) {

    return (
        <div className="layout layout--guest min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100 dark:bg-gray-900 p-10">
            <div className="layout__header">
            </div>
            <Navigation />

            <div className="layout__main w-full mt-6 px-6 py-4 overflow-hidden">
                {children}
            </div>
        </div>
    );
}
