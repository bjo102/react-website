export default function Section({ children, className = ''}) {
    return (
        <section className={`section w-full mt-6 px-6 py-4 bg-white dark:bg-gray-800 shadow-md overflow-hidden sm:rounded-lg${className ? ' ' + className.trim() : ''}`}>
            {children}
        </section>
    )
}
