import InputLabel from "@/Components/InputLabel";
import TextInput from "@/Components/TextInput";
import InputError from "@/Components/InputError";
import PrimaryButton from "@/Components/PrimaryButton";
import {useForm} from "@inertiajs/react";
import {FormEventHandler} from "react";

export default function SubscribeForm({}) {
    const { data, setData, post, processing, errors, reset } = useForm({
        email: '',
        preferences: '',
    });

    const submit: FormEventHandler = (e) => {
        e.preventDefault();
        post(route('register'));
    };

    return (
        <form className="subscription-form" onSubmit={submit}>
            <div>
                <InputLabel htmlFor="email" value="E-Mail" />
                <TextInput
                    id="email"
                    type="email"
                    name="email"
                    value={data.email}
                    className="mt-1 block w-full"
                    autoComplete="username"
                    onChange={(e) => setData('email', e.target.value)}
                    required
                />

                <InputError message={errors.email} className="mt-2" />
            </div>
            <PrimaryButton className="ms-4" disabled={processing}>
                Subscribe
            </PrimaryButton>
        </form>
    )
}
