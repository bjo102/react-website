import { PropsWithChildren } from 'react';

export default function Heading({ children }: PropsWithChildren) {
    return <h1 className="text-gray-100 dark:text-white">{ children }</h1>
}
