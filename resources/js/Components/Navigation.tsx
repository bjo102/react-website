import {Link, Head, usePage} from '@inertiajs/react';
import { PageProps } from '@/types';
import Inspire from './Inspire';
import GuestLayout from "@/Layouts/GuestLayout";

export default function Navigation() {
    const { auth } = usePage().props
    const loggedIn = auth.user !== null
    console.log(usePage().props)

    let linkClass = "p-5 font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500";
    return (
        <div className="sm:fixed sm:top-0 sm:right-0 p-6 text-end z-50">
            {auth.user ? (
                <Link href={route('dashboard')} className={linkClass}>Dashboard</Link>
            ) : (
                <>
                    <Link href={route('login')} className={linkClass}>Log in</Link>
                    <Link href={route('subscribe')} className={linkClass}>Subscribe</Link>
                    <Link href={route('register')} className={linkClass}>Register</Link>
                </>
            )}
        </div>
    )
}
