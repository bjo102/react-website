import { configureStore } from '@reduxjs/toolkit'
import counterReducer from '@/features/counter/counterSlice'
import { usePage } from '@inertiajs/react';
// const { auth } = usePage().props

// console.log('user data from inertiaJS in the redux store file')
// console.log(auth)

const store = configureStore({
    reducer: {
        counter: counterReducer,
    },
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch

export { store as default };
