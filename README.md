## Black Box Legends

Code. Release. Share.

Adventures in the world of web development.

The hope for this project is to provide useful insights into the life and work of programmers (code included).

This is the application which supports the Black Box Legends website.

### Technologies used

- [Laravel](https://laravel.com/)
- [Inertia](https://inertiajs.com/)
- [React](https://react.dev/)
- [Tailwind](https://tailwindcss.com/)
- [Breeze](https://laravel.com/docs/10.x/starter-kits#breeze-and-inertia)
- [Sail](https://laravel.com/docs/10.x/sail)

### Features To Be Included (Work In Progress)

#### Resources

- Articles
- Videos
- Tags (Topics)
- Categories
- Users
- Comments
- Likes
- Inspirational Passages
- Repositories

#### Features

- Authentication
- User Roles
- Search
- Contact Form
- Admin Dashboard
- User Profile/Dashboard

## Documentation

There is a directory containing important information about this project in the `dox` directory.

You will also find useful quick-access commands there.

## Installation

After cloning the repo, the commands necessary for running the site locally are contained in the `run.sh` script in the root directory.
