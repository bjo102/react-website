<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\ArticleController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//// Synchronously...
//Inertia::share('appName', config('app.name'));
//
//// Lazily...
//Inertia::share('user', $cData);

Route::get('/', function () {
    return Inertia::render('Home', [
        'quote' => strip_tags(\Illuminate\Foundation\Inspiring::quote()),
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'canSubscribe' => Route::has('subscribe'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
        'duderino' => 'Duderino 777',
    ]);
});

//Route::get('/dashboard', function () {
//    return Inertia::render('Dashboard');
//})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
//    Route::get('/', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});


Route::prefix('dashboard')->middleware(['auth', 'verified'])->group(function () {
    Route::get('/', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');

    Route::get('/articles', [ArticleController::class, 'adminIndex'])->name('article.index');
    Route::get('/article', [ArticleController::class, 'edit'])->name('article.edit');
});

require __DIR__.'/auth.php';
