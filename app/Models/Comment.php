<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use App\Models\Upvote;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Comment extends Model
{
    use HasFactory;

    protected $table = 'comments';

    protected $fillable = [
        'article_id',
        'user_id',
        'content'
    ];

    public function commentable(): MorphTo
    {
        return $this->morphTo();
    }

    public function upvotes(): MorphMany
    {
        return $this->morphMany(Upvote::class, 'upvotable');
    }
}
