<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Upvote extends Model
{
    use HasFactory;

    protected $table = 'upvotes';

    protected $fillable = [
        'subject',
        'user',
        'type'
    ];

    public function upvotable(): MorphTo
    {
        return $this->morphTo();
    }
}
