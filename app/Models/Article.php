<?php

namespace App\Models;

use App\Builders\ArticleBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Article extends Model
{
    use HasFactory;

    protected $table = 'articles';

    protected $fillable = [
        'name',
        'title',
        'excerpt',
        'content',
        'categories',
        'tags',
        'status',
        'author',
    ];

//    protected $casts = [
//        'categories' => 'array',
//        'tags' => 'array',
//    ];

    public function editions(): hasMany
    {
        return $this->hasMany(Edition::class);
    }

    public function comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function upvotes(): MorphMany
    {
        return $this->morphMany(Upvote::class, 'upvotable');
    }

    public function newEloquentBuilder($query): ArticleBuilder
    {
        return new ArticleBuilder($query);
    }
}
