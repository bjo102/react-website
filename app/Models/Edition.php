<?php

namespace App\Models;

use App\Builders\EditionsBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Edition extends Model
{
    use HasFactory;

    protected $table = 'editions';

    protected $fillable = [
        'article_id',
        'title',
        'excerpt',
        'content',
        'status',
        'author'
    ];

    public function article(): BelongsTo
    {
        return $this->belongsTo(Article::class);
    }

    public function comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function upvotes(): MorphMany
    {
        return $this->morphMany(Upvote::class, 'upvotable');
    }

    public function newEloquentBuilder($query): EditionsBuilder
    {
        return new EditionsBuilder($query);
    }
}
