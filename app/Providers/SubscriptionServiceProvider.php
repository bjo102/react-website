<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\SubscriptionService;
use App\Interfaces\Services\SubscriptionServiceIF;
use App\Repositories\SubscriptionRepo;

class SubscriptionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(SubscriptionServiceIF::class, function($app) {
            return new SubscriptionService(new SubscriptionRepo);
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
