<?php

namespace App\Services;

use App\Interfaces\Services\SubscriptionServiceIF;
use App\Repositories\SubscriptionRepo;

class SubscriptionService implements SubscriptionServiceIF
{
    public function __construct(
        protected SubscriptionRepo $subscriptionRepo
    ) {}

    public function subscribe($user, $categories)
    {
        return 'user subscribed';
    }
}
