<?php

namespace App\Builders;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

class ArticleBuilder extends Builder
{
    public function whereAuthor(User $user): self
    {
        return $this->where('user', $user->id);
    }

    public function withRelations()
    {
        return $this->with('editions')
            ->with('comments')
            ->with('upvotes');
    }
}
