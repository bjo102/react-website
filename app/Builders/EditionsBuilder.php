<?php

namespace App\Builders;
use Illuminate\Database\Eloquent\Builder;
use App\Models\User;

class EditionsBuilder extends Builder
{
    public function whereAuthor(User $user): self
    {
        return $this->where('user', $user->id);
    }

    public function withRelations()
    {
        return $this->with('article')
                    ->with('comments')
                    ->with('upvotes');
    }
}
