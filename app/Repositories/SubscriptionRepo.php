<?php

namespace App\Repositories;

use App\Interfaces\RepositoryIF;
use App\Models\Subscription;
use Illuminate\Database\Eloquent\Collection;


class SubscriptionRepo implements RepositoryIF
{
    public function create(array $args): Subscription
    {
        return Subscription::create($args);
    }

    public function get(int $id): Subscription
    {
        return Subscription::find($id);
    }

    public function list(array $args = []): Collection
    {
        return Subscription::all();
    }

    public function update(int $id, array $args): Subscription
    {
        return Subscription::find($id)->update($args);
    }

    public function delete(int $id): bool
    {
        return Subscription::delete($id);
    }
}
