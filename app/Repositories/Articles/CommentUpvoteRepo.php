<?php

namespace App\Repositories\Articles;

use App\Interfaces\RepositoryIF;
use App\Models\Upvote;
use Illuminate\Database\Eloquent\Collection;

class CommentUpvoteRepo implements RepositoryIF
{
    public function create(array $args): Upvote
    {
        return Upvote::create($args);
    }

    public function get(int $id): Upvote
    {
        return Upvote::find($id);
    }

    public function list(array $args = []): Collection
    {
        return Upvote::all();
    }

    public function update(int $id, array $args): Upvote
    {
        return Upvote::find($id)->update($args);
    }

    public function delete(int $id): bool
    {
        return Upvote::delete($id);
    }
}
