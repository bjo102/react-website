<?php

namespace App\Repositories\Articles;

use App\Interfaces\RepositoryIF;
use App\Models\ArticleUpvote;
use Illuminate\Database\Eloquent\Collection;

class ArticleUpvoteRepo implements RepositoryIF
{
    public function create(array $args): ArticleUpvote
    {
        return ArticleUpvote::create($args);
    }

    public function get(int $id): ArticleUpvote
    {
        return ArticleUpvote::find($id);
    }

    public function list(array $args = []): Collection
    {
        return ArticleUpvote::all();
    }

    public function update(int $id, array $args): ArticleUpvote
    {
        return ArticleUpvote::find($id)->update($args);
    }

    public function delete(int $id): bool
    {
        return ArticleUpvote::delete($id);
    }
}
