<?php

namespace App\Repositories\Articles;

use App\Interfaces\RepositoryIF;
use App\Models\Comment;
use Illuminate\Database\Eloquent\Collection;

class CommentRepo implements RepositoryIF
{
    public function create(array $args): Comment
    {
        return Comment::create($args);
    }

    public function get(int $id): Comment
    {
        return Comment::find($id);
    }

    public function list(array $args = []): Collection
    {
        return Comment::all();
    }

    public function update(int $id, array $args): Comment
    {
        return Comment::find($id)->update($args);
    }

    public function delete(int $id): bool
    {
        return Comment::delete($id);
    }
}
