<?php

namespace App\Repositories\Articles;

use App\Interfaces\RepositoryIF;
use App\Models\Article;
use Illuminate\Database\Eloquent\Collection;

class ArticleRepo implements RepositoryIF
{
    public function create(array $args): Article
    {
        return Article::create($args);
    }

    public function get(int $id): Article
    {
        return Article::find($id);
    }

    public function list(array $args = []): Collection
    {
        return Article::all();
    }

    public function update(int $id, array $args): Article
    {
        return Article::find($id)->update($args);
    }

    public function delete(int $id): bool
    {
        return Article::delete($id);
    }
}
