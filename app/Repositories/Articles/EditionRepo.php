<?php

namespace App\Repositories\Articles;

use App\Interfaces\RepositoryIF;
use App\Models\Edition;
use Illuminate\Database\Eloquent\Collection;

class EditionRepo implements RepositoryIF
{
    public function create(array $args): Edition
    {
        return Edition::create($args);
    }

    public function get(int $id): Edition
    {
        return Edition::find($id);
    }

    public function list(array $args = []): Collection
    {
        return Edition::all();
    }

    public function update(int $id, array $args): Edition
    {
        return Edition::find($id)->update($args);
    }

    public function delete(int $id): bool
    {
        return Edition::delete($id);
    }
}
