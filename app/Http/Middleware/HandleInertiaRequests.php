<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Inertia\Middleware;
use Tightenco\Ziggy\Ziggy;
use App\Models\User;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that is loaded on the first page visit.
     *
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determine the current asset version.
     */
    public function version(Request $request): string|null
    {
        return parent::version($request);
    }

    /**
     * Define the props that are shared by default.
     *
     * @return array<string, mixed>
     */
    public function share(Request $request): array
    {
        // is user logged in
        $user = $request->user();

        if ($user) {
            // get user id
            $userId = $user->id;
            $model = User::find($userId);
            $userData = [
                'loggedIn' => true,
                'user' => $user,
                'userId' => $userId,
                'email' => $user->email,
            ];
        } else {
            $userData = [
                'loggedIn' => false,
                'user' => null
            ];
        }

        return [
            ...parent::share($request),
            'appName' => config('app.name'),
            'auth' => $userData,
            'apiKeys' => [
              'tinyMCE' => env('TINYMCE_API_KEY'),
            ],
            'ziggy' => fn () => [
                ...(new Ziggy)->toArray(),
                'location' => $request->url(),
            ],
        ];
    }
}
