<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Repositories\SubscriptionRepo;
use Inertia\Inertia;
use App\Interfaces\Services\SubscriptionServiceIF;
use Inertia\Response;

class SubscriptionController extends Controller
{
    public function create(): Response
    {
        return Inertia::render('Auth/Subscribe');
    }

    public function store(Request $request, SubscriptionServiceIF $subscriptionService): RedirectResponse
    {
        $request->validate([
            'user_id',
            'preferences',
        ]);

        return redirect(RouteServiceProvider::HOME);
    }

//    public function index()
//    {
//        return Inertia::render('Subscriptions', [
//            'users' => 'hjg'
//        ]);
//    }

//    public function subscribe(SubscriptionServiceIF $subscriptionService)
//    {
//        /**
//         * Testing Service provider
//         */
//        $message = $subscriptionService->subscribe(11, []);
//    }
}
