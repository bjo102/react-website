<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepo;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct(
        protected UserRepo $users
    ) {}
}
