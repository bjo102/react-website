<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Response;
use Inertia\Inertia;

class ArticleController extends Controller
{
    public function edit(Request $request): Response
    {
        return Inertia::render('Article/ArticleEditor', [
//            'mustVerifyEmail' => $request->user() instanceof MustVerifyEmail,
            'status' => session('status'),
        ]);
    }

    public function adminIndex(Request $request): Response
    {
        return Inertia::render('Article/ArticleIndex', [
//            'mustVerifyEmail' => $request->user() instanceof MustVerifyEmail,
            'status' => session('status'),
        ]);
    }
}
