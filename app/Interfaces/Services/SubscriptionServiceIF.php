<?php

namespace App\Interfaces\Services;

use App\Repositories\SubscriptionRepo;

interface SubscriptionServiceIF
{
    public function __construct(SubscriptionRepo $subscriptionRepo);

    public function subscribe(int $user, array $categories);
}
