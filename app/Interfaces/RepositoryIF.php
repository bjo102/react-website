<?php

namespace App\Interfaces;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

interface RepositoryIF
{
    public function create(array $args): Model;

    public function get(int $id): Model;

    public function list(array $args = []): Collection;

    public function update(int $id, array $args): Model;

    public function delete(int $id): bool;
}
