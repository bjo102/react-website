install sail

`composer require laravel/sail --dev`
`php artisan sail:install`
`./vendor/bin/sail up`



# Creating a sail alias

in linux, add this to ~/.bashrc file

`vim ~/.bashrc`

scroll to bottom of file and add this line

`alias sail='[ -f sail ] && sh sail || sh vendor/bin/sail'`

`sail up` instead of `./vendor/bin/sail up`
