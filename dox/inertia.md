## Server side rendering

command inertia to run server-side rendering of HTML content, enhancing SEO

`php/sail artisan inertia:start-ssr`


### Testing

```
# Before starting SSR server
curl localhost:8000 | grep "Laravel News"
# Nothing found

# After starting SSR server
curl localhost:8000 | grep "Laravel News"
# We find some HTML
```
