<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PassageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $passages = [
            [
                'credit' => 'Murphy',
                'passage' => 'Programs will grow to fill all available compile and link time.',
                'date' => null,
            ],
            [
                'credit' => 'Uncle Bob',
                'passage' => 'The only way to go fast is to go well.',
                'date' => null,
            ],
            [
                'credit' => 'Uncle Bob',
                'passage' => 'We are now living in the age of software reuse -- a fullfilment of one of the oldest promises of the obect-oriented model.',
                'source' => 'Clean Architecture',
                'date' => null,
            ],
            [
                'credit' => 'Uncle Bob',
                'passage' => 'The goal of software architecture is to minimize the human resources required to build and maintain the required system.',
            ],
            [
                'credit' => 'Uncle Bob',
                'passage' => 'The granule of reuse is the granule of release.',
            ],
            [
                'credit' => 'Uncle Bob',
                'passage' => 'gather together those things that change at the same times and for the same reasons. Separate those things that change at different times or for different reasons.',
                'source' => 'Clean Architecture',
            ],
            [
                'credit' => 'Uncle Bob',
                'passage' => 'dependencies run in the direction of abstraction.',
                'source' => 'Clean Architecture',
            ],


            [
                'credit' => 'Leonardo Da Vinci',
                'passage' => 'Simplicity is the ultimate sophistication.',
            ],

            [
                'credit' => 'Winston Royce',
                'passage' => '
                    I believe in this concept, but the implementation described above is risky and invites failure. The
                    problem is illustrated in Figure 4. The testing phase which occurs at the end of the development cycle is the
                    first event for which timing, storage, input/output transfers, etc., are experienced as distinguished from
                    analyzed. These phenomena are not precisely analyzable. They are not the solutions to the standard partial
                    differential equations of mathematical physics for instance. Yet if these phenomena fail to satisfy the various
                    external constraints, then invariably a major redesign is required. A simple octal patch or redo of some isolated
                    code will not fix these kinds of difficulties. The required design changes are likely to be so disruptive that the
                    software requirements upon which the design is based and which provides the rationale for everything are
                    violated. Either the requirements must be modified, or a substantial change in the design is required. In effect
                    the development process has returned to the origin and one can expect up to a lO0-percent overrun in schedule
                    and/or costs.'
            ]
        ];

//        DB::table('users')->insert([
//            'name' => Str::random(10),
//            'email' => Str::random(10).'@gmail.com',
//            'password' => Hash::make('password'),
//        ]);
    }
}
