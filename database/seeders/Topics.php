<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class Topics extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $subjects = [
            'component plugin architecture',
            'dependency injection',
            'dependency inversion',

            'component cohesion',
                'the reuse/release equivalence principle',
                'the common closure principle',
                'the common reuse principle',

            'release numbers',

            'role-based access control',
            'component tension diagram',

            'the weekly build',
            'acyclic dependencies principle',
            'eliminating dependency cycles',

            'build & maintenance maps',
            'component dependency graph',

            'stable dependencies principle',
                'stable/unstable components',
                'responsibility & dependency',
                'fan-in, fan-out', // instability (i) | fan-in = external classes depending on component classes | fan-out = external classes that component classes depend on | i = fan-out / (fan-in + fan-out) | i = 0: maximum stability | i = 1: maximum instability'

            'stable abstractions principle',
                'component abstractness', // (a) | a = number of abstract classes & interfaces in a component / total number of classes in a component | a = 0: minimum | a = 1: maximum abstractness',
                'stability & abstractness', // four point graph: 0 0, 0 1, 1 1. 1 0 | Abstractness (Y axis): top (1) = abstract, bottom (0) concrete | Stability (X axis):  left (0) = stable, right (1): volatile
                'Main sequence', // a = abstractness, i = instability

            'testing',
                // Support testing - technology facing
                'static testing',
                    'linting',
                    'type checking', //
                    'static code analysis', // PhpMetrics, Sonar
                'unit testing',
                'integration testing', // mockito, Mocha, DBUnit, REST-Assured
                'system testing',
                'mutation testing', // PITest, StrykerJS

                // Business testing
                'acceptance testing', // User stories, fulfillment of duties

            'test-driven development', // PHPUnit, JUnit, Jest
            'behaviour-driven development',
            'central-transform',// The practical guide to structured design -Meilir page-jones
            'books',
                'object oriented software engineering - ivar jacobson',
                'Growing Object Oriented Software with Tests - Steve Freeman and Nat Pryce',
            'architectures',
                'hexagonal architecture (aka ports and adapters)',
                'DCI',
                'bce',

            'ci/cd',
                'source',
                'build',
                'test',
                'deploy',

            'javascript',
                'prototypes',
                    'property shadowing', // When a property is defined in the prototype chain, it is shadowed by a property with the same name in the object itself
            
            'Humble object pattern', //  - xUnit Patterns, Meszaros, Addison-Wesley
                'Presenter view pattern',
                    'view models',
                'Database Gateways', // 'Patterns of Enterprise Application Architecture - Martin Fowler'
                'Service listeners',
                'strategy pattern',
                'facade pattern',
            ];
    }
}
