<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user');
            $table->string('name', 150)->unique()->index();
            $table->string('title', 150)->nullable();
            $table->string('excerpt', 500)->nullable();
            $table->text('content')->nullable();
            $table->json('categories')->nullable();
            $table->json('tags')->nullable();
            $table->string('status', 50)->default('draft');
            $table->timestamps();

            $table->foreign('user')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('articles');
    }
};
